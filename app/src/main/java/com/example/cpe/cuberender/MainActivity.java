package com.example.cpe.cuberender;

import android.opengl.GLSurfaceView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;


public class MainActivity extends AppCompatActivity {

    private GLSurfaceView mySurfaceView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      //  mySurfaceView = (GLSurfaceView)R.layout.findViewById(R.id.my_surface_view);


       // setContentView(R.layout.activity_main);

        GLSurfaceView view = new GLSurfaceView(this);  // this should not be called , mySurfaceView should be used
        mySurfaceView.setRenderer(new OpenGLRenderer());
        setContentView(mySurfaceView);
    }


}




